﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab3_4
{
    public partial class Form1 : Form
    {
        string from1 = "1";
        string from2 = "3";
        string to1 = "2";
        string to2 = "4";

        double distance1 = 22.9;
        double distance2 = 1.2;

        double BaseFare = 2.50;
        double DistanceCharge =0.81;
        double ServiceFees = 1.75;
        double totalPrice=0;
        double MinimumFare = 5.50;
        DateTime time;

        //DateTime time=new DateTime(2018,11,13,10,00,00);
        


        public Form1()
        {
            InitializeComponent();
            //rdbtnPool.Checked=true;

        }

        private void btnFind_Click(object sender, EventArgs e)
        {

            if (textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("Empty Field");
                if (textBox1.Text == "")
                {
                    textBox1.Focus();

                }
                else if (textBox2.Text == "")
                {
                    textBox2.Focus();
                }
            }
            else
            {

                if (rdbtnPool.Checked == false && rdbtnDirect.Checked == false)
                {
                    MessageBox.Show("Option not selected");
                    groupBox1.Focus();

                }
                else
                {

                    if (textBox1.Text == from1 && textBox2.Text == to1)
                    {
                        if (rdbtnPool.Checked == true)
                        {
                            totalPrice = BaseFare + DistanceCharge * distance1 + ServiceFees;
                            MessageBox.Show("Pool selected price: " + totalPrice);
                            time = System.DateTime.Now;
                            MessageBox.Show("time:" +time.Hour);
                            if ((time.Hour>=10&&time.Hour<=12)|| (time.Hour >= 16 && time.Hour <= 18) || (time.Hour >= 20 && time.Hour <= 21) )
                            {
                                MessageBox.Show("total prices increased by %20");
                                totalPrice *= 1.2;
                                MessageBox.Show("total: "+totalPrice);
                            }


                        }
                        else if (rdbtnDirect.Checked == true)
                        
                        {
                            totalPrice = BaseFare*1.1 + (DistanceCharge * distance1)*1.15 + ServiceFees;
                            MessageBox.Show("Direct selected price: "+totalPrice);
                            time = System.DateTime.Now;
                            MessageBox.Show("time:" + time.Hour);
                            if ((time.Hour >= 10 && time.Hour <= 12) || (time.Hour >= 16 && time.Hour <= 18) || (time.Hour >= 20 && time.Hour <= 21))
                            {
                                MessageBox.Show("total prices increased by %20");
                                totalPrice *= 1.2;
                                MessageBox.Show("total: " + totalPrice);
                            }

                        }
                    }
                    else if (textBox1.Text == from2 && textBox2.Text == to2)
                    {
                        if (rdbtnPool.Checked == true)
                        {
                            totalPrice = BaseFare + DistanceCharge * distance2 + ServiceFees;
                            MessageBox.Show("Pool selected price: " + totalPrice);
                            time = System.DateTime.Now;
                            MessageBox.Show("time:" + time.Hour);

                            if ((time.Hour >= 10 && time.Hour <= 12) || (time.Hour >= 16 && time.Hour <= 18) || (time.Hour >= 20 && time.Hour <= 21))
                            {
                                MessageBox.Show("total prices increased by %20");
                                totalPrice *= 1.2;
                                MessageBox.Show("total: " + totalPrice);
                            }
                        }
                        else if (rdbtnDirect.Checked == true)

                        {
                            totalPrice = BaseFare * 1.1 + (DistanceCharge * distance2) * 1.15 + ServiceFees;
                            MessageBox.Show("Direct selected price: " + totalPrice);
                            time = System.DateTime.Now;
                            MessageBox.Show("time:" + time.Hour);

                            if ((time.Hour >= 10 && time.Hour <= 12) || (time.Hour >= 16 && time.Hour <= 18) || (time.Hour >= 20 && time.Hour <= 21))
                            {
                                MessageBox.Show("total prices increased by %20");
                                totalPrice *= 1.2;
                                MessageBox.Show("total: " + totalPrice);
                            }
                        }

                    }
                    else
                    {
                        MessageBox.Show("Enter a valid location or destination.");
                    }
                }
            }

            if (totalPrice<MinimumFare)
            {
                totalPrice = MinimumFare;

            }
        }



    }
}
